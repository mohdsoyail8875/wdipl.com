const User = require("../Model/User")
const router = require("express").Router()
const bcrypt = require("bcrypt")
const twofactor = require("node-2fa");
const multer = require("multer");
const Auth = require("../Middleware/Auth");
const path = require("path")
const Stripe = require("stripe")

const { v4, uuidv4 } = require("uuid");
const Payment = require("../model/Payment");


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, "public");
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
        cb(null, uniqueSuffix + path.extname(file.originalname));
    },
});
const fileFilter = (req, file, cd) => {
    if (
        (file.mimetype === "image/jpg",
            file.mimetype === "image/jneg",
            file.mimetype === "image/png")
    ) {
        cd(null, true);
    } else {
        cd(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 100000000000,
    },
});



router.post("/register", async (req, res) => {
    var birthday1 = new Date(req.body.DOB);
    function isOverEighteen(birthday) {
        var ageDifMs = Date.now() - birthday.getTime();
        var ageDate = new Date(ageDifMs);
        var age = Math.abs(ageDate.getUTCFullYear() - 1970);
        if (age > 18) {
            return true;
        } else {
            return false;
        }
    }

    const agedata = isOverEighteen(birthday1);

    var regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    var regex2 = /^.{10,10}$/;
    const otpgerate = Math.floor(100000 + Math.random() * 9000)
    console.log(req.body);
    const { Name, Password, Phone, DOB, Location, Email, Cpassword } = req.body
    try {
        let user = await User.findOne({ Phone })
        if (user) {
            return res.send({ msg: "Phone have already" })
        }


        if (!Name) {
            return res.status(200).send({ msg: "Name" })
        }
        if (!regex2.test(Phone)) {
            return res.status(200).send({ msg: "Phone" })
        }
        if (!regex.test(Email)) {
            return res.status(200).send({ msg: "Email" })
        }
        if (!Location) {
            return res.status(200).send({ msg: "Location" })
        }
        if (agedata == false) {
            return res.status(200).send({ msg: "DOB" })
        }
        if (!Password) {
            return res.status(200).send({ msg: "Password" })
        }
        if (Password !== Cpassword) {
            return res.status(200).send({ msg: "New passwords do not match." });
        }

        user = new User({
            Name,
            Password,
            Email,
            DOB,
            Phone,
            Role: "User",
            Location,
            otp: otpgerate

        })

        const salt = await bcrypt.genSalt(10);
        user.Password = await bcrypt.hash(user.Password, salt);


        await user.save()
        res.status(200).send({ msg: "User Created", user })

    } catch (error) {
        res.status(500).json({ ok: false, msg: error.toString() });
    }
});

router.post("/register/finish", async (req, res) => {
    const { Phone, otp } = req.body;
    try {

        let user = await User.findOne({ Phone: Phone })
        // //console.log()(user.otp);
        if (user) {

            const matched = user.otp;
            const token = await user.genAuthToken();

            // //console.log()("sdfkjfksf", matched);
            if (matched == otp) {
                user.IsActive = true
                user.save()
                res.send({ msg: "you are active", token })
            } else {
                res.send({ msg: "you are anctive" })

            }
        }


    } catch (e) {
        console.log(e);
    }
})

router.post("/otp", async (req, res) => {
    const { Phone, Email } = req.body;
    try {
        const otpgerate = Math.floor(100000 + Math.random() * 9000)
        let user = await User.findOne({ $and: [{ Phone: Phone }, { Email: Email }] })
        if (user) {
            user.otp = otpgerate
            user.save()
            res.send({ msg: "otp send" })
        }

    } catch (e) {
        console.log(e);
    }
})

router.post("/login", async (req, res) => {

    console.log(req.body);

    var regex2 = /^.{10,10}$/;
    const Phone = req.body.Phone;
    const Password = req.body.Password;
    try {
        let user = await User.findOne({ Phone: Phone }).where("IsActive").equals(true)

        if (!regex2.test(req.body.Phone)) {
            return res.status(200).send({ msg: "Phone" })
        }
        if (!req.body.Password) {
            return res.status(200).send({ msg: "Password" })
        }
        if (!user) {
            return res.status(200).send({ msg: "no" })
        }
        const isMatch = await bcrypt.compare(Password, user.Password);
        if (!isMatch) {
            return res.status(200).send({ msg: "invalid" });
        }
        const token = await user.genAuthToken();
        res.status(200).send({
            status: 200,
            msg: "login successful",
            token,
            user,

        });

        // console.log(user);
    } catch (err) {
        console.log(err);
        res.status(401).send("Invalid Details");
    }
});

router.get("/user", Auth, async (req, res) => {
    try {
        const data = await User.findById(req.user.id)
        res.status(200).send(data)
    } catch (e) {
        console.log(e);
    }
})

router.post('/logout', Auth, async (req, res) => {
    try {
        // req.user.tokens = req.user.tokens.filter((token) => {
        //     return token.token !== req.token
        // })
        req.user.tokens = []
        await req.user.save()

        res.send()
        console.log(("ok"));
    } catch (e) {
        res.status(500).send()
    }
})



router.post("/upload/file", Auth, upload.fields([{ name: "Vedio", maxCount: 1 }, { name: "Image", maxCount: 1 }]), async (req, res) => {
    try {
        console.log(req.files);


        if (!req.files.Image) {
            return res.send({ msg: "Image" })
        }
        if (!req.files.Vedio) {
            return res.send({ msg: "Vedio" })
        }
        if (!req.body.Gender) {
            return res.send({ msg: "select gender" })
        }
        await User.findByIdAndUpdate(req.user.id, req.body, { new: true })
        const data = await User.findByIdAndUpdate(req.user.id,

            {
                $set: {
                    Image: req.files.Image.map((data1) => data1.filename),
                    Vedio: req.files.Vedio.map((data1) => data1.filename)
                }
            })

        res.status(200).send({ status: true, msg: "Image Upload", data })
    } catch (e) {
        console.log(e);
    }


})


router.post("/profile", Auth, async (req, res) => {
    console.log(req.body);
    try {
        await User.findByIdAndUpdate(req.user.id, req.body, { new: true })

        res.status(200).send({ msg: "Profile Update" })
    } catch (e) {
        console.log(e);
    }
})
router.post("/profile/uploaded", Auth, async (req, res) => {
    console.log(req.body);

    try {
        if (!req.body.BIO) {
            return res.send({ msg: "BIO" })
        }
        if (!req.body.Fb_Url) {
            return res.send({ msg: "Fb_Url" })
        }
        if (!req.body.Link_Url) {
            return res.send({ msg: "Link_Url" })
        }
        await User.findByIdAndUpdate(req.user.id, req.body, { new: true })

        res.status(200).send({ msg: "Profile Update" })
    } catch (e) {
        console.log(e);
    }
})

router.post("/changepassword", Auth, (req, res) => {
    const { Password, newPassword, confirmNewPassword } = req.body;
    const userID = req.user.id;
    let errors = [];
    //Check required fields
    if (!Password || !newPassword || !confirmNewPassword) {
        res.send({ msg: "Please fill in all fields." });
    }

    //Check passwords match

    if (newPassword !== confirmNewPassword) {
        res.send({ msg: "New passwords do not match." });
    }

    //Check password length
    if (newPassword.length < 6 || confirmNewPassword.length < 6) {
        res.send({ msg: "Password should be at least six characters." });
    }



    User.findOne({ _id: userID }).then(user => {

        bcrypt.compare(Password, user.Password, (err, isMatch) => {
            if (err) {
                console.log(err);
                res.send({ msg: "Password Dont" });
            }
            if (isMatch) {

                bcrypt.genSalt(10, (err, salt) =>
                    bcrypt.hash(newPassword, salt, (err, hash) => {
                        if (err) {
                            console.log(err);
                            res.send({ msg: "Password Dont" });
                        }
                        user.Password = hash;
                        user.save();

                    })
                );
                res.send({ msg: "Password Update" });


                // res.redirect("/dashboard");
            }
        });
    }).catch((e) => {
        console.log(e);
    })
    // res.send({ msg: "Please Contact to Admin" });


});



const stripe = Stripe("sk_test_51M9vLVSECEOfAkVUuHtT6Pylg0QX7Jn8UW0jdXzaaUb226UuTGeS8z7JFvLQJdzw032rEB1gPslaIVfsJ661Twhm00NcC4REOy")
router.post("/pay", Auth, async (req, res) => {

    let { amount, id } = req.body;
    console.log(amount.data);

    try {
        const payment = await stripe.paymentIntents.create({
            amount: amount.data,
            currency: "inr",
            description: "Your Company Description",
            payment_method: id,
            confirm: true,

        })
        const payment2 = new Payment({
            User_id: req.user.id,
            Payment_id: payment.id,
            amount: payment.amount,
            status: payment.status,
        })
        
        await payment2.save()
        // console.log(res);

        res.json({
            message: "Payment Successful",
            success: true,
        });

        console.log("DSFsd", payment.id);
    } catch (error) {

        res.json({
            message: "Payment Failed",
            success: false,
        });
    }
});


module.exports = router