const mongoose = require("mongoose")

const PaymentSchema = new mongoose.Schema({
    User_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    Payment_id: {
        type: String
    },
    amount: {
        type: String
    },
    status: {
        type: String
    }
})


const Payment = mongoose.model("StripePayment", PaymentSchema)

module.exports = Payment