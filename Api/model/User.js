const mongoose = require("mongoose")
const jwt = require('jsonwebtoken')


const UserSchema = new mongoose.Schema({
    Role: {
        type: String,
        required: true
    },
    IsActive: {
        type: Boolean,
        default: false
    },
    otp: {
        type: String,
        required: true
    },
    Name: {
        type: String,
        required: true
    },
    Phone: {
        type: Number,
        required: true
    },
    Email: {
        type: String,
        required: true
    },
    Password: {
        type: String,
        required: true
    },
    DOB: {
        type: String,
        required: true
    },
    Location: {
        type: String,
        required: true
    },
    Image: [{
        type: String,

    }],
    Vedio: [{
        type: String,

    }],
    Gender: {
        type: String,
        default: null
    },
    BIO: {
        type: String
    },
    Fb_Url: {
        type: String
    },
    Link_Url: {
        type: String
    },
    reason1: {
        type: String
    },
    reason2: {
        type: String
    },
    reason3: {
        type: String
    },
    reason4: {
        type: String
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]

}, { timestamps: true })




UserSchema.methods.genAuthToken = async function () {
    const user = this;
    const token = jwt.sign({ _id: user._id.toString() }, "soyal");
    user.tokens = user.tokens.concat({ token })
    await user.save()
    return token;
};

const User = mongoose.model("User", UserSchema)

module.exports = User