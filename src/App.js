

import axios from "axios";
import { useEffect, useState } from "react";
import {
  BrowserRouter,
  Routes,
  Route,
  Navigate,

} from "react-router-dom";
import Change from "./Components/Change";
import Delete from "./Components/Delete";
import Desc from "./Components/Desc";
import Home from "./Components/Home";
import Login from "./Components/Login";
import Logout from "./Components/Logout";
import Makepay from "./Components/Makepay";
import Register from "./Components/Register";
import Setting from "./Components/Setting";



function App() {


  const [User, setUser] = useState()
  const access_token = localStorage.getItem('token')
  const userData = () => {
    const headers = {
      Authorization: `Bearer ${access_token}`,
    }
    axios.get(`http://localhost:5000/user`, { headers })
      .then((res) => {
        console.log(res.data.Name);
        setUser(res.data)


      }).catch((e) => {
        if (e.response.status == 401) {
          window.location.href = "/"
        }
        console.log(e);
      })
  }


  // useEffect(() => {
  //   userData()
  // }, [])


  return (
    <>
      <div>
        {access_token ?
          <Logout /> :
          ""
        }
      </div>
      <BrowserRouter>

        {!access_token ?
          <Routes>
            <Route path="/" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route exact path="/login" element={<Login />} />

          </Routes> :
          <Routes>

            <Route path="/home" element={<Home />} />
            <Route path="/Profile" element={<Desc />} />
            <Route path="/setting" element={<Setting />} />
            <Route path="/payment" element={<Makepay User={User} />} />
            <Route path="/change" element={<Change />} />
            <Route path="/delete" element={<Delete />} />
            <Route path="/" element={<Register />} />
            <Route path="/login" element={<Login />} />
            <Route exact path="/login" element={<Login />} />
          </Routes>
        }






      </BrowserRouter >
    </>
  );
}

export default App;