import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Header from './Header';

function Setting() {
    const navigate = useNavigate();

    const [User, setUser] = useState()
    const [Name, setName] = useState()
    const [Email, setEmail] = useState()
    const [DOB, setDOB] = useState()
    const [Location, setLocation] = useState()


    const userData = () => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.get(`http://localhost:5000/user`, { headers })
            .then((res) => {
                console.log(res.data);
                setUser(res.data)
                setName(res.data.Name)
                setEmail(res.data.Email)
                setDOB(res.data.DOB)
                setLocation(res.data.Location)

            }).catch((e) => {
                console.log(e);
            })
    }

    const UpdatePrpfile = () => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }



        axios.post("http://localhost:5000/profile", {
            DOB,
            Name,
            Email,
            Location,
        }, { headers }).then((res) => {

            console.log(res);
            userData()
        }).catch((e) => {
            if (e.response.status == 401) {
                localStorage.removeItem('token');
                window.location.href = "/"
            }
            console.log(e);

        })
    }

    const payment = () => {
        navigate("/payment")
    }
    const deleteAc = () => {
        navigate("/delete")
    }
    const Chnage = () => {
        navigate("/change")
    }
    const setting = () => {
        navigate("/setting")
    }


    useEffect(() => {
        userData()
    }, [])
    return (
        <div>

            <h2 style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: "",
                marginTop: "50px"

            }}>Welcome {User && User.Name}</h2>
            <div>

                <Header />

                <div>
                    <div style={{
                        display: "flex", alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: "",

                    }}>
                        <div style={{ display: "flex", flexDirection: "column", marginTop: "100px" }}>
                            <div><h6 >Full Name</h6 ></div>
                            <input type="text" style={{ transition: 'all 3s ease-out 0s', borderRadius: "40px", marginRight: "20px", width: "100%" }}
                                value={Name} onChange={(e) => setName(e.target.value)} ></input>
                            <div style={{ marginTop: "4px" }}><h6 >DOB</h6 ></div>
                            <input type="date" style={{ transition: 'all 3s ease-out 0s', borderRadius: "40px", marginRight: "20px", width: "100%" }}
                                value={DOB} onChange={(e) => setDOB(e.target.DOB)} ></input>
                        </div>
                        <div style={{ display: "flex", flexDirection: "column", marginTop: "100px" }}>
                            <div style={{ marginTop: "4px", marginLeft: "20px", }}><h6 >Email</h6 ></div>
                            <input type="email" style={{ transition: 'all 3s ease-out 0s', borderRadius: "40px", marginLeft: "20px", width: "100%" }}
                                value={Email} onChange={(e) => setEmail(e.target.value)} ></input>
                            <div style={{ marginTop: "4px", marginLeft: "20px", }}><h6 >Location</h6 ></div>
                            <input placeholder='djf' style={{ transition: 'all 3s ease-out 0s', borderRadius: "40px", marginLeft: "20px", width: "100%" }}
                                value={Location} onChange={(e) => setLocation(e.target.value)} ></input>
                        </div>

                    </div>
                    <div onClick={UpdatePrpfile} style={{ marginLeft: "600px", marginTop: "20px" }}>
                        <button className="form-control"
                            style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat", width: "100px" }}
                            
                        >Update</button>
                    </div>

                </div>
            </div>
        </div >
    )
}

export default Setting