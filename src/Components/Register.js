import axios from 'axios'
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

function Register() {

    const [Name, setName] = useState()
    const [Email, setEmail] = useState()
    const [Password, setPassword] = useState()
    const [Phone, setPhone] = useState()
    const [DOB, setDOB] = useState()
    const [Location, setLocation] = useState()
    const [Cpassword, setCPassword] = useState()
    const [otp, setotp] = useState()

    const [show, setShow] = useState(false)

    const navigate = useNavigate();
    const Register = (e) => {
        e.preventDefault()
        axios.post("http://localhost:5000/register", {
            Name,
            Phone,
            Email,
            Password,
            Cpassword,
            DOB,
            Location
        }).then((res) => {

            if (res.data.msg == "Phone have already") {
                Swal.fire('Phone Number Already Used ')
            }
            if (res.data.msg == "Name") {
                Swal.fire('Enter Name')
            }
            if (res.data.msg == "Phone") {
                Swal.fire('Enter Valid Phone')
            }
            if (res.data.msg == "Email") {
                Swal.fire('Enter Valid Email')
            }
            if (res.data.msg == "Location") {
                Swal.fire('Enter Location')
            }
            if (res.data.msg == "DOB") {
                Swal.fire('Greater then 18 DOB')
            }
            if (res.data.msg == "Password") {
                Swal.fire('Enter Password')
            }
            if (res.data.msg == "New passwords do not match.") {
                Swal.fire('Password Must be same')
            }
            if (res.data.msg == "User Created") {
                setShow(true)
            }
        }).catch((e) => {
            console.log(e);
        })
    }


    const VarifyUser = () => {
        axios.post("http://localhost:5000/register/finish", {
            otp,
            Phone
        }).then((res) => {
            console.log(res.data.token);
            if (res.data.msg == "you are active") {
                localStorage.setItem("token", res.data.token)
                window.location.href = "home"
            }
        })
    }

    const otpsendAPi = () => {
        axios.post("http://localhost:5000/otp", {
            otp,
            Phone,
            Email
        }).then((res) => {
            console.log(res.data.token);
            if (res.data.msg == "otp send") {
                Swal.fire('OTP send Email ID and Phone Number ')
            }
        })
    }
    return (
        <div>
            {show == false && <div style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                height: '100vh', padding: "20px", backgroundColor: ""
            }}>
                <form >
                    <div>
                        <div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Name</div>
                                </div>
                                <input className="form-control" type="text" placeholder="Full Name"
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                    onChange={(e) => setName(e.target.value)}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Phone No</div>
                                </div>
                                <input className="form-control" name="" type="number" placeholder="Phone Number"
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                    onChange={(e) => setPhone(e.target.value)}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Email</div>
                                </div>
                                <input className="form-control" name="" type="email" placeholder="Email Address"
                                    onChange={(e) => setEmail(e.target.value)}
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>DOB</div>
                                </div>
                                <input className="form-control" name="" type="date" placeholder="DOB"
                                    onChange={(e) => setDOB(e.target.value)}
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Location</div>
                                </div>
                                <input className="form-control" name="text" type="text" placeholder="Location"
                                    onChange={(e) => setLocation(e.target.value)}
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Password</div>
                                </div>
                                <input className="form-control" name="mobile" type="text" placeholder="Password"
                                    onChange={(e) => setPassword(e.target.value)}
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>C Pass</div>
                                </div>
                                <input className="form-control" name="mobile" type="mobile" placeholder="C Password"
                                    onChange={(e) => setCPassword(e.target.value)}
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>

                            <div onClick={Register} className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px', backgroundColor: "black" }}>
                                <div className="input-group-prepend">

                                </div>
                                <button className="form-control" name="mobile" type="mobile" placeholder="Email Address"
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat" }}
                                    
                                >Register</button>
                            </div>
                            <div style={{ color: "red" }}>Have To Accoutn <Link to="/login" >Login</Link></div>
                        </div>
                    </div>
                </form>
                <div style={{ marginLeft: "30px", }}>
                    <div>
                        <div >
                            <h1> Instructions for Candidate</h1>
                        </div>
                        <div style={{ marginTop: "50px" }}>
                            <h6> Add Proper Validation on the fields</h6>
                        </div>
                        <div style={{ marginTop: "40px" }}>
                            <h6> DOB Should be greater then then 18  Year</h6>
                        </div>
                        <div style={{ marginTop: "40px" }}>
                            <h6> Google Location Api to be used location</h6>
                        </div>
                        <div style={{ marginTop: "40px" }}>
                            <h6>Password Should Encrypted  and  saved in DB</h6>
                        </div>

                    </div>
                </div>

            </div >
            }

            {show &&
                <>
                    <div style={{ margin: "auto", width: "500px", marginTop: "15px" }}>
                        <h3>Welcome {Name}</h3>
                    </div>
                    <div style={{ margin: "auto", width: "500px", marginTop: "70px" }}>
                        <h5>Please Enter OTP</h5>
                    </div>
                    
                    <div style={{ width: "500px", margin: "auto", marginTop: "30px" }}>

                        <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                            <div className="input-group-prepend">
                                <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>OTP</div>
                            </div>
                            <input className="form-control" type="text" placeholder="Enter OTP"
                                style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", marginLeft: "20px" }}
                                onChange={(e) => setotp(e.target.value)}
                            />


                        </div>
                        <div style={{ width: "90px", marginTop: "20px" }}>
                            <button className="form-control" name="mobile" type="mobile" placeholder="Email Address"
                                style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat" }}
                                onClick={VarifyUser}
                            >Verify</button>
                        </div>
                    </div>
                    <div style={{ margin: "auto", width: "500px", marginTop: "15px", display: "flex", alignItems: 'center', }}>
                        Din't Receive OTP ?
                        <div onClick={otpsendAPi} style={{ color: "blue" }}> Resend OTP</div>
                    </div>
                </>

            }

        </div>
    )
}

export default Register