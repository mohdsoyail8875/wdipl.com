import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'
import Header from './Header'



function Change() {


    const [Password, setPassword] = useState()
    const [newPassword, setnPassword] = useState()
    const [confirmNewPassword, setcPassword] = useState()


    const resetPasword = () => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.post("http://localhost:5000/changepassword", {
            newPassword,
            confirmNewPassword,
            Password,
        }, { headers }).then((res) => {
            if (res.data.msg == "Password should be at least six characters.") {
                Swal.fire('Password should be at least six characters.')
            }
            if (res.data.msg == "Password Dont") {
                Swal.fire('Password Do not Match')
            }
            if (res.data.msg == "Please Contact to Admin") {
                Swal.fire('Please Contact to Admin')
            }
            if (res.data.msg == "Please fill in all fields.") {
                Swal.fire('Please fill in all fields.')
            }
            if (res.data.msg == "New passwords do not match.") {
                Swal.fire('New passwords do not match.')
            }
            if (res.data.msg == "Password Update") {
                Swal.fire('Password Updated Successfully')
            }
        }).catch((e) => {
            if (e.response.status == 401) {
                localStorage.removeItem('token');
                window.location.href = "/"
            }
        })
    }

    const navigate = useNavigate();
    const [User, setUser] = useState()
    const access_token = localStorage.getItem('token')
    const userData = () => {
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.get(`http://localhost:5000/user`, { headers })
            .then((res) => {
                console.log(res.data);
                setUser(res.data)


            }).catch((e) => {
                if (e.response.status == 401) {
                    window.location.href = "/"
                }
                console.log(e);
            })
    }


    useEffect(() => {
        userData()
    }, [])


    return (
        <div>

            <h2 style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: "",
                marginTop: "50px"

            }}>Welcome {User && User.Name}</h2>
            <div>

                <Header />
                <div>

                    <div>
                        <div style={{
                            display: "flex", alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: "",
                            marginTop: "50px"
                        }}>
                            <div>

                                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', }}>
                                    <div className="input-group-prepend">
                                        <div className="input-group-text" style={{ width: '150px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Password</div>
                                    </div>
                                    <input className="form-control" type="text" placeholder="Enter Password"
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                        onChange={(e) => setPassword(e.target.value)}
                                    />
                                </div>
                                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', }}>
                                    <div className="input-group-prepend">
                                        <div className="input-group-text" style={{ width: '150px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>New Password</div>
                                    </div>
                                    <input className="form-control" type="text" placeholder="New Password"
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                        onChange={(e) => setnPassword(e.target.value)}
                                    />
                                </div>
                                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', }}>
                                    <div className="input-group-prepend">
                                        <div className="input-group-text" style={{ width: '150px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>C Password</div>
                                    </div>
                                    <input className="form-control" type="text" placeholder="C password"
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                        onChange={(e) => setcPassword(e.target.value)}
                                    />
                                </div>
                                <div style={{ marginLeft: "600px", marginTop: "20px" }}>
                                    <button className="form-control"
                                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat", width: "100px" }}
                                        onClick={resetPasword}
                                    >Update</button>
                                </div>
                            </div>

                        </div >



                    </div>
                </div>
            </div>
        </div >
    )
}

export default Change