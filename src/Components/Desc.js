import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

function Desc() {

    const navigate = useNavigate();

    const [BIO, setBIO] = useState()
    const [Link_Url, setLiUrl] = useState()
    const [Fb_Url, setFburl] = useState()

    const [User, setUser] = useState()
    const userData = () => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.get(`http://localhost:5000/user`, { headers })
            .then((res) => {
                console.log(res.data.BIO);
                // setUser(res.data)

                setBIO(res.data.BIO)
                setLiUrl(res.data.Link_Url)
                setFburl(res.data.Fb_Url)


            }).catch((e) => {
                console.log(e);
            })
    }

    const ProfileUpload = (e) => {
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }



        axios.post("http://localhost:5000/profile/uploaded", {
            BIO,
            Link_Url,
            Fb_Url,
        }, { headers }).then((res) => {

            if (res.data.msg == "BIO") {
                Swal.fire('Add Bio')
            }
            if (res.data.msg == "Fb_Url") {
                Swal.fire('Enter Fb URL')
            }
            if (res.data.msg == "Link_Url") {
                Swal.fire('Enter Link Url')
            }
            if (res.data.msg == "Profile Update") {
                console.log(res);
                navigate("/setting")
            }
        }).catch((e) => {
            if (e.response.status == 401) {
                localStorage.removeItem('token');
                window.location.href = "/"
            }
        })


    }

    useEffect(() => {
        userData()
    }, [])
    return (
        <div style={{
            display: "flex", alignItems: 'center',
            justifyContent: 'center',
            height: '100vh', padding: "20px", backgroundColor: "",

        }}>
            <div>

                <textarea className="input-group" type="text" placeholder="bio" style={{ transition: 'top 0.5s ease 0s', width: "500px", height: "80px" }}
                    value={BIO}
                    onChange={(e) => setBIO(e.target.value)}
                >
                </textarea>


                <div style={{ marginTop: "50px" }}>LinkedIn URL</div>
                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>

                    <input className="form-control" name="asd" type="text" placeholder="www.text.com"
                        value={Link_Url}
                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                        onChange={(e) => setLiUrl(e.target.value)}
                    />
                </div>
                <div style={{ marginTop: "50px" }}>Facebook URL</div>
                <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>

                    <input className="form-control" name="asd" type="text" placeholder="www.text.com"
                        value={Fb_Url}
                        onChange={(e) => setFburl(e.target.value)}
                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}

                    />
                </div>



                <div onClick={ProfileUpload} className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', backgroundColor: "black" }}>
                    <div className="input-group-prepend">

                    </div>
                    <button className="form-control"
                        style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat" }}
                        
                    >Submit</button>
                </div>


            </div>
            <div style={{ marginLeft: "30px", }}>
                <div>
                    <div >
                        <h1> Instructions for Candidate</h1>
                    </div>
                    <div style={{ marginTop: "10px" }}>
                        <h6> BIO must be only 200 world</h6>
                    </div>
                    <div style={{ marginTop: "10px" }}>
                        <h6> Add Proper Validation on the fields</h6>
                    </div>


                </div>
            </div>
        </div >
    )
}

export default Desc