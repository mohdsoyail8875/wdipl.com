import React, { useEffect, useState } from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { Link, useNavigate } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Modal from 'react-bootstrap/Modal';
import { CheckoutForm } from "./CheckoutForm";
import StripeContainer from "./StripeContainer";
import Header from "./Header";
import axios from "axios";


let Data = [
    {
        Price: "100",
        Name: "Plane A"
    },
    {
        Price: "1000",
        Name: "Plane B"
    },
    {
        Price: "1100",
        Name: "Plane C"
    },
    {
        Price: "1002",
        Name: "Plane D"
    },
    {
        Price: "1003",
        Name: "Plane H"
    },

]



function Makepay() {

    const [User, setUser] = useState()
    const access_token = localStorage.getItem('token')
    const userData = () => {
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.get(`http://localhost:5000/user`, { headers })
            .then((res) => {
                console.log(res.data);
                setUser(res.data)


            }).catch((e) => {
                if (e.response.status == 401) {
                    window.location.href = "/"
                }
                console.log(e);
            })
    }


    useEffect(() => {
        userData()
    }, [])


    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    var Publishable_Key = 'pk_test_51M9vLVSECEOfAkVU55qdckSwX5hDGceCeFYl5yuM1hshq5bP4u7LwoAb18QbwEHZfsCVKii5Ye8cnGSYugsevwiQ00DVGvGNQV'


    const stripeTestPromise = loadStripe(Publishable_Key);

    return (
        <div>

            <h2 style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: "",
                marginTop: "50px"

            }}>Welcome {User && User.Name}</h2>

            <div>
                <Header />

                <div>
                    <div style={{
                        display: "flex", alignItems: 'center',
                        justifyContent: 'center',
                        backgroundColor: "",


                    }}>

                        {Data && Data.map((data) => {
                            return (
                                <Card style={{
                                    width: '100rem',
                                    height: "10rem",
                                    display: "flex", alignItems: 'center',
                                    justifyContent: 'center',
                                    backgroundColor: "",
                                    marginRight: "3px",
                                    marginTop: "40px"

                                }} key={data.Price}>


                                    <div style={{ marginTop: "10px" }}>
                                        <h4>{data.Name}</h4>
                                    </div>
                                    <div style={{ marginTop: "10px" }}>

                                        <h5>$ {data.Price}</h5>
                                    </div>


                                    <Button variant="primary" onClick={handleShow}>
                                        Pay
                                    </Button>

                                    <Modal show={show} onHide={handleClose}>
                                        <Modal.Header closeButton>
                                            <Modal.Title>Enter Your Card Details</Modal.Title>
                                        </Modal.Header>
                                        <Elements stripe={stripeTestPromise}>
                                            <StripeContainer data={data.Price} />
                                        </Elements>
                                        <Modal.Footer>
                                            <Button variant="secondary" onClick={handleClose}>
                                                Close
                                            </Button>
                                        </Modal.Footer>
                                    </Modal>


                                </Card>
                            )
                        })}


                    </div>
                </div>
            </div>
        </div >
    )
}

export default Makepay