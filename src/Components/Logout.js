import axios from 'axios'
import React from 'react'
import { useNavigate } from 'react-router-dom';

function Logout() {


    const LogoutApi = () => {
        let access_token = localStorage.getItem("token")
        const headers = {
            Authorization: `Bearer ${access_token}`
        }
        axios.post(`http://localhost:5000/logout`, {
            headers: headers
        }, { headers })
            .then((res) => {
                // setUser(res.data)
                localStorage.removeItem("token");
                localStorage.removeItem("token");
                localStorage.removeItem("token");
                window.location.href = '/'

            }).catch((e) => {
                // alert(e.msg)
            })
    }



    return (
        <div >
            <button onClick={(e) => LogoutApi(e)}>logout</button>
        </div>
    )
}

export default Logout