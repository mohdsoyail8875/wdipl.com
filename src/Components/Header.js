import React from 'react'
import { useNavigate } from 'react-router-dom';

function Header() {
    const navigate = useNavigate();
    const payment = () => {
        navigate("/payment")
    }
    const deleteAc = () => {
        navigate("/delete")
    }
    const Chnage = () => {
        navigate("/change")
    }
    const setting = () => {
        navigate("/setting")
    }
    return (
        <div><h2 style={{
            display: "flex", alignItems: 'center',
            justifyContent: 'center',
            backgroundColor: "",
            marginTop: "100px"

        }}>
            <div style={{ marginLeft: "120px" }} onClick={setting}>Profile Details</div>
            <div style={{ marginLeft: "120px" }} onClick={Chnage}>Change Password</div>
            <div style={{ marginLeft: "120px" }} onClick={deleteAc}>Delete Account</div>

            <div style={{ marginLeft: "120px" }} onClick={payment}>Make Payment</div>
        </h2></div>
    )
}

export default Header