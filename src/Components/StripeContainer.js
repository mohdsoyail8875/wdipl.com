import React from "react";
import { loadStripe } from "@stripe/stripe-js";
import { Elements,PaymentElement } from "@stripe/react-stripe-js";
import { CheckoutForm } from "./CheckoutForm";





var Publishable_Key = 'pk_test_51M9vLVSECEOfAkVU55qdckSwX5hDGceCeFYl5yuM1hshq5bP4u7LwoAb18QbwEHZfsCVKii5Ye8cnGSYugsevwiQ00DVGvGNQV'


const stripeTestPromise = loadStripe(Publishable_Key);

const StripeContainer = (data) => {
    return (
        <Elements stripe={stripeTestPromise}>
            <CheckoutForm data={data}/>
        </Elements>
    );
};

export default StripeContainer;