import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

function Home() {
    const [Image, setImage] = useState(null)
    const [Vedio, setVedio] = useState(null)
    const [Gender, setGender] = useState()

    const navigate = useNavigate();

    const uploadfile = (e) => {
        e.preventDefault();
        const formData = new FormData();

        formData.append("Image", Image);
        formData.append("Vedio", Vedio);
        formData.append("Gender", Gender);
        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }

        axios.post(`http://localhost:5000/upload/file`, formData, { headers })
            .then((res) => {
                console.log(res);
                if (res.data.msg == "select gender") {
                    Swal.fire('select Gender')
                }
                if (res.data.msg == "Image") {
                    Swal.fire('upload Image')
                }
                if (res.data.msg == "Vedio") {
                    Swal.fire('upload Vedio')
                }
                if (res.data.msg == "Image Upload") {
                    console.log(res);
                    navigate("/Profile")
                }

            }).catch((e) => {
                if (e.response.status == 401) {
                    localStorage.removeItem('token');
                    window.location.href="/"
                  }
                  console.log(e);
            })

    }

    useEffect(() => {
        const frontPhoto = document.getElementById('frontPhoto');
        frontPhoto.onchange = e => {
            const [file] = frontPhoto.files;
            setImage(file)
        }
        const backPhoto = document.getElementById('backPhoto');
        backPhoto.onchange = e => {
            const [file] = backPhoto.files;
            setVedio(file)
        }
    }, [])

    return (
        <div>
            <div style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                height: '100vh', padding: "20px", backgroundColor: ""
            }}>
                <div>
                    <div>
                        <div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de', marginRight: "20px" }}>Image</div>
                                <div className="input-group-prepend">
                                </div>
                                <input className="form-control"
                                    id="frontPhoto" name="frontPhoto" type="file" accept="image/*" required

                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de', marginRight: "20px" }}>Video</div>
                                </div>
                                <input className="form-control" id="backPhoto" name="backPhoto" type="file" accept="image/*" required
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                />
                            </div>
                            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                                <div className="input-group-prepend">
                                    <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de', marginRight: "20px" }}>Gender</div>
                                </div>
                                <div> <input type="radio" id="age1" name="age" value="30"
                                    onChange={(e) => setGender("Male")}
                                /> Male</div>

                                <div style={{ marginLeft: "20px" }}> <input type="radio" id="age1" name="age" value="30"
                                    onChange={(e) => setGender("Female")}
                                />Female</div>


                            </div>


                            <div onClick={uploadfile} className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px', backgroundColor: "black" }}>
                                <div className="input-group-prepend">

                                </div>
                                <button className="form-control"
                                    style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat" }}

                                    
                                >Proceed</button>
                            </div>

                        </div>
                    </div>
                </div>


            </div >


        </div>
    )
}

export default Home