import React from "react";
import { CardElement, PaymentElement, useStripe, useElements } from "@stripe/react-stripe-js";
import axios from "axios";
import Swal from "sweetalert2";
import { useNavigate } from "react-router-dom";

export const CheckoutForm = (data) => {
    const stripe = useStripe();
    const elements = useElements();
    const navigate = useNavigate();

    const handleSubmit = async (event) => {
        event.preventDefault();
        const { error, paymentMethod } = await stripe.createPaymentMethod({
            type: "card",
            card: elements.getElement(CardElement),
        });

        if (!error) {
            console.log("Stripe 23 | token generated!", paymentMethod);
            const access_token = localStorage.getItem('token')
            const headers = {
                Authorization: `Bearer ${access_token}`,
            }
            try {
                const { id } = paymentMethod;
                await axios.post(
                    "http://localhost:5000/pay",
                    {
                        amount: data.data,
                        id: id,
                    },
                    { headers }
                ).then((response) => {
                    if (response.data.success) {
                        console.log("CheckoutForm.js 25 | payment successful!");
                        console.log("data", response.data.success);

                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })

                        setTimeout(() => {
                            navigate("/setting")
                        }, 1000)
                    }
                })


            } catch (error) {
                console.log("CheckoutForm.js 28 | ", error);
            }
        } else {
            console.log(error.message);
        }
    };

    return (
        <form onSubmit={handleSubmit} style={{ maxWidth: 400, height: "200px" }}>
            <CardElement style={{ height: "200px" }} />
            <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px', }}>
                <div className="input-group-prepend">

                </div>
                <button className="form-control"
              
                    style={{ marginTop: "20px", width: "10px", transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "blue", color: "wheat" }}

                >Pay</button>
            </div>
        </form>
    );
};