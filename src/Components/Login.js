import axios from 'axios'
import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

function Login() {


    const [Password, setPassword] = useState()
    const [Phone, setPhone] = useState()

    const navigate = useNavigate();
    const LoginApi = () => {

        axios.post("http://localhost:5000/login", {
            Phone,
            Password,
        }).then((res) => {

            if (res.data.msg == "no") {
                Swal.fire('user do not have ')
            }
            if (res.data.msg == "Phone") {
                Swal.fire('Enter 10 Digit Number')
            }
            if (res.data.msg == "invalid") {
                Swal.fire('invalid')
            }
            if (res.data.msg == "Password") {
                Swal.fire('Enter Password')
            }
            if (res.data.msg == "login successful") {
                window.location.href = "/home"
                console.log(res.data.token);
                localStorage.setItem("token", res.data.token)
            }
        }).catch((e) => {
            console.log(e);
        })
    }
    return (
        <div style={{
            display: "flex", alignItems: 'center',
            justifyContent: 'center',
            height: '100vh', padding: "20px", backgroundColor: ""
        }}>
            <div>
            
                <div>
                    <div>
                        <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                            <div className="input-group-prepend">
                                <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Phone No</div>
                            </div>
                            <input className="form-control" name="mobile" type="number" placeholder="Phone"
                                style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                onChange={(e) => setPhone(e.target.value)}
                            />
                        </div>
                        <div className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px' }}>
                            <div className="input-group-prepend">
                                <div className="input-group-text" style={{ width: '90px', backgroundColor: '#e9ecef', border: '1px solid #d8d6de' }}>Password</div>
                            </div>
                            <input className="form-control" name="mobile" type="text" placeholder="Password"
                                style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px" }}
                                onChange={(e) => setPassword(e.target.value)}

                            />
                        </div>



                        <div onClick={LoginApi} className="input-group mb-2 " style={{ transition: 'top 0.5s ease 0s', top: '5px', backgroundColor: "black" }}>
                            <div className="input-group-prepend">

                            </div>
                            <button className="form-control" name="mobile" type="mobile" placeholder="Email Address"
                                style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat" }}
                                
                            >Login</button>
                        </div>
                        <div style={{ color: "red" }}>New Register <Link to="/" >Register</Link></div>
                    </div>
                </div>
            </div>
            <div style={{ marginLeft: "30px", }}>
                <div>
                    <div >
                        <h1> Instructions for Candidate</h1>
                    </div>
                    <div style={{ marginTop: "50px" }}>
                        <h6> Add Proper Validation on the fields</h6>
                    </div>
                    <div style={{ marginTop: "40px" }}>
                        <h6> DOB Should be greater then then 18  Year</h6>
                    </div>
                    <div style={{ marginTop: "40px" }}>
                        <h6> Google Location Api to be used location</h6>
                    </div>
                    <div style={{ marginTop: "40px" }}>
                        <h6>Password Should Encrypted  and  saved in DB</h6>
                    </div>

                </div>
            </div>
        </div >
    )
}

export default Login