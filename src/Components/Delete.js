import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Header from './Header';



function Delete() {


    const navigate = useNavigate();

    const delectUser = () => {

        const access_token = localStorage.getItem('token')
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.post("http://localhost:5000/profile", {
            IsActive: false,
            reason1: "personal issue",
            reason2: "time waste",
            reason3: "education life",
            reason4: "hate this side",
        }, { headers }).then((res) => {
            console.log(res);
            localStorage.removeItem('token');
            window.location.href = "/"
        }).catch((e) => {
            if (e.response.status == 401) {
                localStorage.removeItem('token');
                window.location.href = "/"
            }
        })
    }

    const [User, setUser] = useState()
    const access_token = localStorage.getItem('token')
    const userData = () => {
        const headers = {
            Authorization: `Bearer ${access_token}`,
        }
        axios.get(`http://localhost:5000/user`, { headers })
            .then((res) => {
                console.log(res.data);
                setUser(res.data)


            }).catch((e) => {
                if (e.response.status == 401) {
                    window.location.href = "/"
                }
                console.log(e);
            })
    }


    useEffect(() => {
        userData()
    }, [])
    return (
        <div>

            <h2 style={{
                display: "flex", alignItems: 'center',
                justifyContent: 'center',
                backgroundColor: "",
                marginTop: "50px"

            }}>Welcome {User && User.Name}</h2>
            <div>
                <Header />
                <div>
                    <div style={{
                        marginLeft: "500px"

                    }}>
                        <div>
                            <input type="checkbox" /> Reason 1
                        </div>
                        <div>
                            <input type="checkbox" /> Reason 2
                        </div>
                        <div>
                            <input type="checkbox" /> Reason 3
                        </div>
                        <div>
                            <input type="checkbox" /> Reason 4
                        </div>

                        <button className="form-control"
                            style={{ transition: 'all 3s ease-out 0s', borderRadius: "4px", backgroundColor: "black", color: "wheat", width: "100px" }}

                            onClick={delectUser}
                        >Delete</button>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default Delete